#!/usr/bin/env fish

switch $SHELL
	case '*/zsh'
		set TOOLS_HOME (readlink -f (dirname "0"))
	case '*/bash'
		set TOOLS_HOME (readlink -f (dirname "BASH_SOURCE[0]"))
	case '*'
		echo no home
end

alias cdtools="cd $TOOLS_HOME"
alias vibash="vim $TOOLS_HOME/bashrc; and . $TOOLS_HOME/bashrc"
alias vifish="vim $TOOLS_HOME/y.fish; and . $TOOLS_HOME/y.fish"
alias vivim="vim $TOOLS_HOME/vimrc"

set SHRUGGY '¯\_(ツ)_/¯'

#requires x or y parameters
alias pane="tmux resize-pane"
alias a="git add -u; and git status"
alias c="git commit"
alias indev="git commit -m'in-development'"
alias t="tig --all"
alias g=git
alias v=vim
alias pd="pushd"

function fif
	if test (count $argv) -lt 3
		echo "fif usage: fif directory extension pattern"
		return -1
	end

	if test "$argv[2]" = '.*'; or test "$argv[2]" = '\*'
		find "$argv[1]" -type f -iname "*$argv[2]" -exec egrep --color -in "$argv[3..]" {} \+
	else
		find "$argv[1]" -type f -iname "*.$argv[2]" -exec egrep --color -in "$argv[3..]" {} \+
	end
end

function repos
	if test (count $argv) -lt 1
		for i in */
			pushd "$i"
			test -d .git; and echo $i; and git status
			popd
		end
	else
		for i in */
			pushd "$i"
			test -d .git; and echo $i; and $argv[1..]
			popd
		end
	end
end

function gdate
	if test (count $argv) -lt 1
		echo "Error: Require 1 argument"
		exit -1
	end
	set GIT_AUTHOR_DATE `date -d "$argv[1]" +%s`
	set GIT_COMMITTER_DATE "$GIT_AUTHOR_DATE"
	unset GIT_AUTHOR_DATE
	unset GIT_COMMITTER_DATE
end

function gauthor
	if test (count $argv) -lt 2
		echo "Error: Require 2 arguments"
		exit -1
	end
	set GIT_AUTHOR_NAME "$argv[1]"
	set GIT_AUTHOR_EMAIL "$argv[2]"
	set GIT_COMMITTER_NAME "$GIT_AUTHOR_NAME"
	set GIT_COMMITTER_EMAIL "$GIT_AUTHOR_EMAIL"
	if test (count $argv) -gt 2
		set GIT_AUTHOR_DATE `date -d "$argv[3]" +%s`
		set GIT_COMMITTER_DATE "$GIT_AUTHOR_DATE"
	end
end

