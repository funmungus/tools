if [ $# -eq 0 ]
then
  echo "Invalid arguments" >&2
  exit 1
elif [ $# -eq 1 ]
then
  ffmpeg -i "$1" -c copy -map 0:a "$1.mp4"
else
  ffmpeg -i "$1" -c copy -map 0:a "$2"
fi
