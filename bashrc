#!/usr/bin/env bash
case $SHELL in
*/zsh)
export TOOLS_HOME=$(readlink -f $(dirname "${0}"))
;;
*/bash)
export TOOLS_HOME=$(readlink -f $(dirname "${BASH_SOURCE[0]}"))
;;
*)
esac
alias cdtools="cd ${TOOLS_HOME}"
alias vibash="vim ${TOOLS_HOME}/bashrc && . ${TOOLS_HOME}/bashrc"
alias vivim="vim ${TOOLS_HOME}/vimrc"

export SHRUGGY='¯\_(ツ)_/¯'

#requires x or y parameters
alias pane="tmux resize-pane"
alias a="git add -u && git status"
alias c="git commit"
alias indev="git commit -m'in-development'"
alias t="tig --all"
alias g=git
alias status="git status"
alias v=vim
alias pd="pushd"
alias icat="kitty +kitten icat"

fif() {
	if [[ $# -lt 3 ]]; then
		echo "fif usage: fif directory extension pattern"
		return -1
	fi

	if [[ "$2" == .* ]] || [[ "$2" == \* ]]; then
		find "$1" -type f -iname "*$2" -exec egrep --color -in "${*:3}" {} \+
	else
		find "$1" -type f -iname "*.$2" -exec egrep --color -in "${*:3}" {} \+
	fi
}

repos() {
	if [[ $# -lt 1 ]]; then
		for i in */; do
			(cd "$i" && [[ -d .git ]] && echo $i && git status)
		done
	else
		for i in */; do
			(cd "$i" && [[ -d .git ]] && echo $i && ${*:1})
		done
	fi
}

gdate() {
	if [[ $# -lt 1 ]]; then
		echo "Error: Require 1 argument"
		exit -1
	fi
	GIT_AUTHOR_DATE=`date -d "$1" +%s`
	GIT_COMMITTER_DATE="$GIT_AUTHOR_DATE"
	export GIT_AUTHOR_DATE
	export GIT_COMMITTER_DATE
}

gauthor() {
	if [[ $# -lt 2 ]]; then
		echo "Error: Require 2 arguments"
		exit -1
	fi
	export GIT_AUTHOR_NAME="$1"
	export GIT_AUTHOR_EMAIL="$2"
	export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
	export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"
	if [[ $# -gt 2 ]]; then
		export GIT_AUTHOR_DATE=`date -d "$3" +%s`
		export GIT_COMMITTER_DATE="$GIT_AUTHOR_DATE"
	fi
}

#parse_git_branch() {
#     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
#}

