so <sfile>:p:h/plug.vim

function! ApplySettings()
	set number
	set rnu
	set mouse=a
	set splitbelow
	set splitright
	syntax on
	" I like 4 space tabs
	autocmd Syntax javascript setlocal ts=4 sw=4 noexpandtab
	autocmd Syntax yaml setlocal ts=2 sw=2 expandtab
	autocmd Syntax c,cpp,h,hxx call CppSyntax()
	autocmd Syntax qml setlocal ts=4 sw=4 noexpandtab
	autocmd BufRead,BufNewFile *.gd setlocal ts=4 sw=4 noexpandtab | call GdCommands()
	autocmd Syntax json setlocal ts=2 sw=2 expandtab
	autocmd Syntax ruby setlocal ts=2 sw=2 expandtab
	autocmd Syntax python setlocal ts=4 sw=4 expandtab | Python3Syntax
	autocmd BufRead,BufNewFile *.mcr setlocal filetype=json | setlocal syntax=json

	au VimEnter * RainbowParenthesesToggle
	au Syntax * RainbowParenthesesLoadRound
	au Syntax * RainbowParenthesesLoadSquare
	au Syntax * RainbowParenthesesLoadBraces

	" Zap all trails
	highlight trailingSpaces ctermbg=red guibg=red
	set hlsearch
	match trailingSpaces /\s\+$/
endfunction

function! GdCommands()
	inoremap <buffer> <F5> <esc>:w<cr>:GodotRun<cr>i
	nmap <buffer> ,b :w<cr>:GodotRun<cr>
endfunction

function! VerifyCppSyntax()
	if &syntax == "c" || &syntax == "cpp"
		call CppSyntax()
	endif
endfunction

function! CppSyntax()
	" set syntax=cpp.doxygen
	setlocal ts=4 sw=4 noexpandtab
	syntax sync fromstart
endfunction

if has("gui_running")
	set background=dark
	set termguicolors
	" colorscheme deep-space
	winpos 0 27
	set guifont=Fira\ Mono\ 12 lines=28 columns=9999
endif

let g:netrw_browse_split = 0
let g:netrw_winsize = 20

nmap ! :!
nmap q :q!<CR>
nmap <s-q> :qa!<CR>
nmap <silent> <c-s> :w<CR>
vmap <silent> ,f y/<c-r>0<CR>
vmap <silent> ,,f y:vim /<c-r>0/ ***
nmap <silent> ,f yiw/<c-r>0<CR>
nmap <silent> ,<s-f> yiW/<c-r>0<CR>
nmap <silent> ,,f yiw:vim /<c-r>0/ ***
nmap <silent> ,,<s-f> yiW:vim /<c-r>0/ ***
vmap <silent> ,/ y/<c-r>0<CR>
vmap <silent> ,,/ y:vim /<c-r>0/ ***
nmap <silent> ,/ yiw/<c-r>0<CR>
nmap <silent> ,<s-/> yiW/<c-r>0<CR>
nmap <silent> ,,/ yiw:vim /<c-r>0/ ***
nmap <silent> ,,<s-/> yiW:vim /<c-r>0/ ***
nnoremap <silent> <CR> :noh<CR><CR>
inoremap <silent> <c-c> <Esc>:noh<CR>
inoremap <silent><expr> <c-@> coc#refresh()

" Tabs
nmap <silent> <F2> :Tex<CR><c-g>
nmap <silent> <F3> :TagbarToggle<CR>
nmap <silent> <F5> :e<CR><c-g>
nmap <silent> <F6> :bp<CR><c-g>
nmap <silent> <F7> :bn<CR><c-g>
nmap <silent> <F8> :prev<CR><c-g>
nmap <silent> <F9> :n<CR><c-g>

" Quickfix
nmap <silent> <F10> :cp<CR><c-g>
nmap <silent> <F11> :cn<CR><c-g>
nmap <silent> [q :cfirst<CR><c-g>
nmap <silent> ]q :clast<CR><c-g>

" Splits
nmap <silent> <C-J> :winc j<CR> :res<CR> :vertical res<CR>
nmap <silent> <C-K> :winc k<CR> :res<CR> :vertical res<CR>
nmap <silent> <C-L> :winc l<CR> :res<CR> :vertical res<CR>
nmap <silent> <C-H> :winc h<CR> :res<CR> :vertical res<CR>

nmap <silent> <c-p> :CtrlP<CR>
nmap <silent> ,. :Ex<CR>
nmap <silent> ,r :Rex<CR>
nmap <silent> ,l :Lex<CR>
nmap <silent> ,n :Sex<CR>
nmap <silent> ,m :Sex!<CR>
nmap <silent> ,t :Tex<CR>

windo call ApplySettings()
windo call VerifyCppSyntax()
tabdo call ApplySettings()
tabdo call VerifyCppSyntax()

let g:coc_global_extensions = ['coc-json', 'coc-git', 'coc-html', 'coc-highlight', 'coc-prettier']

call plug#begin('~/.vim/plugged')

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
" Plug 'junegunn/vim-easy-align'

" On-demand loading
" Plug 'scrooloose/syntastic'
Plug 'tyrannicaltoucan/vim-deep-space'
Plug 'airblade/vim-rooter'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'majutsushi/tagbar'
Plug 'vhdirk/vim-cmake'
Plug 'bfrg/vim-cpp-modern'
Plug 'fedorenchik/qt-support.vim'
Plug 'peterhoeg/vim-qml'
Plug 'neomake/neomake'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-commentary'
Plug 'kien/rainbow_parentheses.vim'
Plug 'christoomey/vim-sort-motion'
Plug 'christoomey/vim-system-copy'
Plug 'altercation/vim-colors-solarized'
Plug 'tpope/vim-rails'
Plug 'vim-python/python-syntax'
Plug 'habamax/vim-godot'
Plug 'udalov/kotlin-vim'
Plug 'neoclide/coc.nvim', { 'branch': 'release', 'do': { -> coc#util#install() }}
Plug 'petrbroz/vim-glsl'
Plug 'vim-scripts/oceandeep'
" windows example
" let g:godot_executable = 'C:/Path/To/Godot/godot.exe'
" OSX example
" let g:godot_executable = '/Applications/Godot.app'
if has("unix")
	let g:godot_executable = '/usr/local/bin/godot'
endif

" Initialize plugin system
call plug#end()
" TODO: CocInstall coc-godot coc-kotlin-dev coc-bash arduino 
