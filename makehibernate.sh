#!/usr/bin/env bash
# We want to hibernate and resume from the swapfile, /swapfile. Every time
# the kernel or systemd boot is updated the resume command is removed.  Here
# is a script to update the boot configuration with resume and resume_offset commands.
filename="$1"
if [[ -z "$filename" ]]; then
	filename=/boot/efi/loader/entries/$(sudo awk '$1 ~ /default/ {print $2}' /boot/efi/loader/loader.conf).conf
fi
_UUID=$(findmnt -no UUID -T /swapfile)
_OFFSET=$(sudo filefrag -v /swapfile | awk '$1 ~ /^0:/ {gsub(/\.+/, " "); print $4}')
# echo filename $filename
# echo uuid $_UUID
# echo offset $_OFFSET
sudo sed -i 's/\(root=UUID='$_UUID' ro\)/\1 resume=UUID='$_UUID' resume_offset='$_OFFSET'/g' "$filename"
